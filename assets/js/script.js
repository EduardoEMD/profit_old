$(function(){


	$('.modal').modal();
  $('select').material_select();

  var end = new Date('07/10/2017 3:0 AM');
  var timer;

  function countdown() {

    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var _day = _hour * 24;

    var now = new Date();
    var distance = end - now;
    if (distance < 0) {

      clearInterval(timer);
      return;
    }
    var days = Math.floor(distance / _day);
    var hours = Math.floor((distance % _day) / _hour);
    var minutes = Math.floor((distance % _hour) / _minute);
    var seconds = Math.floor((distance % _minute) / _second);

    var time = [days+'<br> <sup>օր</sup>',hours+'<br> <sup>ժամ</sup>',minutes+'<br> <sup>րոպե</sup>',seconds+'<br> <sup>վրկ</sup>']
    var lines = $('.time-box footer').find('h4')

    time.forEach(function(item,index){
      lines[index].innerHTML = item    
    })


  }

  timer = setInterval(countdown, 1000);

  var setInputRangeMaxAndMin = function(){
    let element = document.querySelector('.abt-footer')
    var maxScrollLeft = element.scrollWidth - element.clientWidth - 45;
    $('#scroll-images').attr('max',maxScrollLeft)
  }
  var setDivWith = function(){
    let arts = $('.abt-ft-scroll').find('article')
    $('.abt-ft-scroll').css('width',(arts.length+1)*260+"px")
    setInputRangeMaxAndMin()
  }()
  $('.logo img').click(function() {
    $(".button-collapse i").trigger("click");
  });

  $(".button-collapse").sideNav();

  $('.slider').slider({
    interval:2000,
    transition:800,
    height:450
  });
   $('.button-collapse').sideNav({
      menuWidth: 300, // Default is 300
      edge: 'left', // Choose the horizontal origin
      closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
      draggable: true, // Choose whether you can drag to open on touch screens,
     
    }
  );
     
  var lesson;
  $('.les-text p:last-child()').click(function(){
    lesson = $(this).attr('data-lsMore')
    $('.lessons_list').fadeOut()

    setTimeout(()=>{
      $('.lesson-more[data-lsMore='+lesson+']').fadeIn()
      
    },300)
    $('html, body').animate({
      scrollTop: $(".lessons_list").offset().top-100
    });

  })
  $('.lesson-more > i').click(function(){
    $('.lesson-more[data-lsMore='+lesson+']').fadeOut()

    setTimeout(()=>{
      $('.lessons_list').fadeIn()

      $('html, body').animate({
        scrollTop: $(".lessons_list").offset().top-100
      });
      
      
    },300)
  })

  $(".ls" ).on({
    mouseenter:function(){
     $(this).find('.must-roll').addClass('active-rotate')
   },
   mouseleave:function(){
     $(this).find('.must-roll').removeClass('active-rotate')
   }

 })
  $('#scroll-images').on('input', function () {
    let pos = $('#scroll-images').val()
    $('.abt-footer').animate({scrollLeft:pos},0)
  });

  $('.abt-icon').click(function(){

   let info = $(this).attr('data-info')
   let divs = $('.abt-txt-holder').find('.abt-text')
   divs.each(function(){
    if(this.outerHTML.indexOf(info) !== -1 ){
     $(this).fadeIn('slow')
   }else{
     $(this).hide()
   }

 })

   $('html, body').animate({
    scrollTop: $(".abt-txt-holder").offset().top-300
  });
 })

  $('button[data-target="modal1"],.cd-footer').click(function(){

   let lesson = $(this).attr('data-lesson')

   $('.modal-content del').html(lesson)

 })


  $('.reg_to_all').click(function(){

    var name = $('#first_name').val(),
        email = $('#email').val(),
        phone = $('#phone').val(),
        text = $('#text').val(),
        lesson = $('.modal-content del').html();

    let result = sendReg(name,email,phone,lesson);

    if(result != undefined)
       Materialize.toast(result, 4000)
    

  })

  $('.register').click(function(){
    var name = $('#name').val(),
        email = $('#reg_email').val(),
        mobile = $('#reg_phone').val(),
        text = $('#text').val(),
        lesson = $('.modal-content del').html();
    
    sendReg(name,email,mobile,lesson,text);
    

   
   

  })

   var list  = $(this).find('*[tabindex]').sort(function(a,b){ return a.tabIndex < b.tabIndex ? -1 : 1; }),
        first = list.first();
    list.last().on('keydown', function(e){
        if( e.keyCode === 9 ) {
            first.focus();
            return false;
        }
    });


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
  function sendReg(name,email,mobile,lesson,text = ""){
          if(!name || !mobile){
            $(".error").html("* Խնդրում ենք պարտադիր լրացնել անուն եւ հեռախոսահամար դաշտերը");
            Materialize.toast("Խնդրում ենք պարտադիր լրացնել անուն եւ հեռախոսահամար դաշտերը", 4000)
            return false;

          }
          else if(!$.isNumeric(mobile)){
            $(".error").html("* Հեռախոսահամար դաշտում պետք է գրված լինեն միայն թվեր");
            Materialize.toast("Հեռախոսահամար դաշտում պետք է գրված լինեն միայն թվեր", 4000)
            return false;


          }else if(mobile.length <= 5){
            $(".error").html("* Հեռախոսահամար դաշտում պետք է գրված լինի առնվազն 6 նիշ");
            Materialize.toast("Հեռախոսահամար դաշտում պետք է գրված լինի առնվազն 6 նիշ", 4000)
            return false;



          }else if(email && validateEmail(email) == false){
            $(".error").html("* Մուտքագրված էլ-փոստի հասցեն սխալ է");
            Materialize.toast("Մուտքագրված էլ-փոստի հասցեն սխալ է", 4000)
            return false;

          }
          else{
            $(".error").empty().hide();

            var data = {
              name: name,
              mobile: mobile,
              text:text, 
              email:email,
              lesson:lesson
            }

          $.post($('#base').val() + "/app/register", data).then(function(r){

              console.log(r)
              $(".error").html("Շնորհակալություն ծառայությունից օգտվելու համար: Մեր մասնագետը կկապվի Ձեզ հետ")
              $(".error").css("color", "green").show();

          })

          }
          
      

 }

 var directionsDisplay;
 var directionsService = new google.maps.DirectionsService();
 var map;

 function myMap() {
  directionsDisplay = new google.maps.DirectionsRenderer();
  var mapProp= {
    center:new google.maps.LatLng(40.1637336, 44.5129472),
    zoom:15,
  };
  map = new google.maps.Map(document.getElementById("map"),mapProp);
  directionsDisplay.setMap(map);
  directionsDisplay.setOptions( { suppressMarkers: true } );

  // Add Marker Function
  function addMarker(props){
    var marker = new google.maps.Marker({
      position:props.coords,
      map:map,
      animation:google.maps.Animation.BOUNCE,
      //icon:props.iconImage
    });

      // Check for customicon
      if(props.iconImage){
      // Set icon image
      marker.setIcon(props.iconImage);
    }

      // Check content
      if(props.content){
        var infoWindow = new google.maps.InfoWindow({
          content:props.content
        });

        marker.addListener('mouseover', function(){
          infoWindow.open(map, marker);
        });
      }
    }

    var contentString = "<div class='map-content'>";
    contentString += "<h5> Պրոֆայթի ուսումնական կենտրոն </h5>";

    contentString += "<h6> Տիգրան Մեծ մասնաճյուղ </h6><br>";
    contentString += "<p> <small>Տիգրան Մեծ 49, 4-րդ հարկ</small></p><br>";
    contentString += "<img width='150' height='150' src='http://profitdeco.com/img/sil.jpg'> ";

    contentString += "</div>";

    var contentString2 = "<div class='map-content'>";
    contentString2 += "<h5> Պրոֆայթի ուսումնական կենտրոն </h5>";

    contentString2 += "<h6> Մոսկովյան մասնաճյուղ</h6>";
    contentString2 += "<p> <small>ՀԱՅՔ ամբողջագիտական համալսարան, 4-րդ հարկ</small></p><br>";

    contentString2 += "<img width='150' height='150' src='http://profitdeco.com/img/hayq.jpg'> ";

    contentString2 += "</div>";


    var image = {
      url: 'http://icons.iconarchive.com/icons/icons-land/vista-map-markers/256/Map-Marker-Marker-Outside-Chartreuse-icon.png', 
      scaledSize : new google.maps.Size(40, 45)
    };
    var markers = [
    {
      coords:{lat:40.1637336,lng:44.5129472},
      
      iconImage:image,
      content: contentString
    },
    {
      coords:{lat:40.184246,lng:44.5220000},
      content: contentString2
    }

    ];

    for(var i = 0;i < markers.length;i++){
      addMarker(markers[i]);
    }


  }

  myMap();
  var input = document.getElementById('map-search');
  var searchBox = new google.maps.places.SearchBox(input);

  function calcRoute(x, y) {

    var origin = new google.maps.LatLng(x.lat, x.lng);
    var destination = new google.maps.LatLng(y.lat, y.lng);
    var request = {
      origin: origin,
      destination: destination,
      travelMode: google.maps.TravelMode['WALKING']
    };
    directionsService.route(request, function(response, status) {
      if (status == 'OK') {
        directionsDisplay.setDirections(response);
      }
    });
  }

  $('.show-direction').on('click',function(){
    $('.progress').fadeIn()
    clearInterval(displayDirection);
    var a = $('#our-crd').val()
    var b = $('.user-pos input').val()
    if( a && b){

      var x,y;
      addressConvert(a,function(r){
        x = r;

      })
      addressConvert(b,function(r){
        y = r;

      })
      var displayDirection =  setInterval(function(){
        if(x && y){
          calcRoute(x,y)
          clearInterval(displayDirection);
          $('.progress').hide(100)
        }
      },1000)
    }
    else{
      $('.progress').hide(100)
      Materialize.toast('Խնդրում ենք, նշեք Ձեր գտնվելու վայրը', 4000)


    }
  })





  function addressConvert(address = 'new yourk',callback){

    var geocoder = new google.maps.Geocoder();

    geocoder.geocode( { 'address': address}, function(results, status) {

      if (status == google.maps.GeocoderStatus.OK) {

        var latitude = results[0].geometry.location.lat();
        var longitude = results[0].geometry.location.lng();

        callback({lat:latitude,lng:longitude})

      } 

    }); 

  }
  $(".dropdown-button").dropdown();

  $(".dropdown-button").click(()=>{
    $('#dropdown1').css("width","auto")
  })

  $('.abt-icon').click(function(){

    $('.abt-icon').each(function(){
      $(this).removeClass('abt-icon-active')
    })
    
    $(this).addClass('abt-icon-active')
  })

  $('*[data-to]').click(function(){
    var to = $(this).attr('data-to')

    scrollTo(to)

  })

  $('#to-top').click(()=>{
     scrollTo('.slider',2000)
  })

  $(window).scroll(function(){

    let scroll = $(window).scrollTop()

    if(scroll > 1300){

      $('#to-top').css('opacity',.5)

    }else{

      $('#to-top').css('opacity',0)

     } 

    if(scroll > 250){

        $('nav').css('height','80px')
    }else{

        $('nav').css('height','131px')

    }    

  })

  function scrollTo(elm = 'body',time = 1000){

    $('html, body').animate({
        scrollTop: $(elm).offset().top - 150
    }, time);

  }


})
