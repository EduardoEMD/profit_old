<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Պրոֆայթի ուսումնական կենտրոն</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
  <link rel="shortcut icon" href="img/logo.png">
  <link rel="shortcut icon" href="img/logo.png">
  <link rel="icon" type="text/css" href="<?=base_url()?>/assets/img/logo.jpg">
  <link rel="icon" type="image/png" href="/path/to/icons/favicon-192x192.png" sizes="192x192">
  <link rel="apple-touch-icon" sizes="180x180" href="/path/to/icons/apple-touch-icon-180x180.png">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url().'/assets/css/style.css' ?>">

 </head>
<body>

    
  <ul id="dropdown1" class="dropdown-content" style="width: auto !important;">
    <li><a data-to="#registration">ամբողջական կուրս</a></li>
    <li><a data-to="#halfpension">մասնակի ուսուցում</a></li>
  </ul>
	<nav>
    <div class="nav-wrapper white">
      <a href="#!" class="logo">
      	<img src="<?=base_url()?>/assets/img/logo.png">
      </a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="hide-on-med-and-down nav-b">
        <li><a data-to=".slider">ԳԼԽԱՎՈՐ</a></li>
        <li><a data-to="#lessons_list">ԴԱՍԸՆԹԱՑՆԵՐ</a></li>
        <li><a data-to="#feedback">ՄԵՐ ՈՒՍԱՆՈՂՆԵՐԸ</a></li>
        <li><a data-to="#reg-cards">ԱՌԱՋԻԿԱ ԴԱՍԸՆԹԱՑՆԵՐ</a></li>
        <li><a class="dropdown-button"  data-activates="dropdown1">ԳՐԱՆՑՈՒՄ</a></li>
        <li><a data-to="#find-us">ԻՆՉՊԵՍ ԳՏՆԵԼ ՄԵԶ</a></li>
        
      </ul>
      <ul class="side-nav" id="mobile-demo">
        <li><a data-to=".slider">ԳԼԽԱՎՈՐ</a></li>
        <li><a data-to="#lessons_list">ԴԱՍԸՆԹԱՑՆԵՐ</a></li>
        <li><a data-to="#feedback">ՄԵՐ ՈՒՍԱՆՈՂՆԵՐԸ</a></li>
        <li><a data-to="#reg-cards">ԱՌԱՋԻԿԱ ԴԱՍԸՆԹԱՑՆԵՐ</a></li>
        <li><a data-to="#registration">ԳՐԱՆՑՈՒՄ</a></li>
        <li><a data-to="#find-us">ԻՆՉՊԵՍ ԳՏՆԵԼ ՄԵԶ</a></li>
      </ul>
    </div>
  </nav>

  <div id="main-content">

   <div class="slider">
    <ul class="slides">
      <li>
        <img src="<?= base_url().'/assets/img/White.jpg' ?>">
        <div class="caption center-align">
        </div>
      </li>
      <li>
        <img src="<?= base_url()?>/assets/img/slides/s.png"> 
        <div class="caption left-align">
          <h3 class="r-heading ">Որակյալ կրթություն </h3>
          <h5 class="light grey-text text-lighten-3 res-p text-left" >Խորացված ուսուցում</h5>
        </div>
      </li>
      <li>
        <img src="<?= base_url()?>/assets/img/slides/p3.png"> 
        <div class="caption right-align">
          <h3 class="r-heading ">Պրակտիկա</h3>
          <h5 class="light grey-text text-lighten-3 res-p text-right" >Գործնական հմտությունների զարգացում</h5>
        </div>
      </li>
      <li>
        <img src="<?= base_url()?>/assets/img/slides/s3.png">
        <div class="caption center-align">
          <h3 class="r-heading ">Աշխատանք և կարիերա</h3>
          <h5 class="light grey-text text-lighten-3 res-p ">Ուսման վարձի վերադարձ</h5>
        </div>
      </li>
    </ul>
  </div>


   <div class="lessons">
   		<h3 class="center r-heading">Ի՞նՉ ԿԱՐՈՂ ԵՔ ՍՈՎՈՐԵԼ ՄԵԶ ՄՈՏ</h3>
      <h5 class="center flow-text">Մեր ամբողջական դասընթացը բաղկացած է 2 փուլից՝ </h5>
      
      

      <div class="lesson-intro">
      <section class="ls-i1">
        <p class="ls-type res-p"><b>Հիմնական կուրսը</b></p>
        <p class="ls-type res-p">Ուսանողները խորությամբ ծանոթանում են` HTML/HTML5, CSS/CSS3, JavaScript, ECMAScript6, jQuery, PHP/OOP, MySQL, AJAX, VCS, MVC & Codeigniter տեխնոլոգիրաներին, ստեղծում են տարբեր պրոյեկտներ՝ զարգացնելով գործնական հմտությունները: Հիմնական կուրսի տևողությունը 6 ամիս է, ամսավճարը 30.000 դրամ, շաբաթական դասերի քանակը՝ 3, յուրաքանչյուր դասի տևողությունը՝ 2 ժամ:</p>
      </section>

      <section class="ls-i2">
        <p class="ls-type res-p"><b>Ընդլայնված կուրսը</b></p>
        <p class="ls-type res-p">Այս կուրսի նպատակն է ուսանողներին ծանոթացնել Wordpress, Laravel, Angular.js, Node.js տեխնոլոգիաներին՝ շեշտադրումը կատարելով առավելապես պրակտիկ աշխատանքների վրա: Ընդլայնված կուրսի տևողությունը 2 ամիս է, ամսավճարը՝ 40.000 դրամ, շաբաթական դասերի քանակը՝ 3, յուրաքանչյուրը՝ 2 ժամ:</p>
      </div>
      </section>
      <p class="ls-type res-p"> <i> Դուք կարող եք ընտրել նաև առանձին տեխնոլոգիաներ և մասնակցել միայն դրանց վերաբերվող դասընթացներին:  </i> </p>      

      <h5 class="center flow-text">Ի՞ՆՉ ԿԱՐՈՂ ԵՔ ՍՈՎՈՐԵԼ ՄԵԶ ՄՈՏ</h5>
         
   		<div class="lessons_list" id="lessons_list">
   			<div class="ls" >
   				<div class="hvr-eff">
   					<img class="img-res" src="<?=base_url()?>assets/img/lessons/1.jpg" class="img">

   				</div>
   				<div class="les-text" id="halfpension">
   					<b class="big">HTML,  CSS</b>
   					<p class="cdt">Տևողությունը 1 ամիս <br> ուսման վարձը 30.000 դրամ  </p>
            <p data-lsMore="html">ուսումնական պլան</p>
   				</div>
  			</div>
        <div class="ls">
          <div class="hvr-eff">
            <img class="img-res" src="<?=base_url()?>assets/img/lessons/2.jpg" class="img">

          </div>
          <div class="les-text">
            <b class="big">JavaScript, jQuery</b>
           <p class="cdt">Տևողությունը 2 ամիս <br> ուսման վարձը 60.000 դրամ  </p>
           <p  data-lsMore="js">ուսումնական պլան</p>
          </div>
        </div>
            <div class="ls" >
          <div class="hvr-eff">
            <img class="img-res" src="<?=base_url()?>assets/img/lessons/3.jpg" class="img">

          </div>
          <div class="les-text" >
            <b class="big">ECMAScript6, OOP</b>
            <p class="cdt">Տևողությունը 1 ամիս <br> ուսման վարձը 30.000 դրամ  </p>
            <!-- <p data-lsMore="es6">ուսումնական պլան</p> -->
          </div>
        </div>
        <div class="ls">
          <div class="hvr-eff">
            <img class="img-res" src="<?=base_url()?>assets/img/lessons/4.jpg" class="img">

          </div>
          <div class="les-text">
            <b class="big">Angular2, TypeScript</b>
            <p class="cdt">Տևողությունը 1 ամիս <br> ուսման վարձը 30.000 դրամ  </p>
            <p  data-lsMore="ng2">ուսումնական պլան</p>
          </div>
        </div>
        <div class="ls" >
          <div class="hvr-eff">
            <img class="img-res" src="<?=base_url()?>assets/img/lessons/5.jpg" class="img">

          </div>
          <div class="les-text">
            <b class="big">Node.js, React.js</b>
            <p class="cdt">Տևողությունը 2 ամիս <br> ուսման վարձը 60.000 դրամ  </p>
            <p data-lsMore="node">ուսումնական պլան</p>
          </div>
        </div>
        <div class="ls" >
          <div class="hvr-eff">
            <img class="img-res" src="<?=base_url()?>assets/img/lessons/6.jpg ?>" class="img">

          </div>
          <div class="les-text">
            <b class="big">PHP, MySQL</b>
            <p class="cdt">Տևողությունը 2 ամիս <br> ուսման վարձը 60.000 դրամ  </p>
            <p data-lsMore="php">ուսումնական պլան</p>
          </div>
        </div>
        <div class="ls" >
          <div class="hvr-eff">
            <img class="img-res" src="<?=base_url()?>assets/img/lessons/7.jpg" class="img">

          </div>
          <div class="les-text">
            <b class="big">MVC, Codeigniter</b>
            <p class="cdt">Տևողությունը 1 ամիս <br> ուսման վարձը 30.000 դրամ  </p>
            <p data-lsMore="mvc">ուսումնական պլան</p>
          </div>
        </div>
        <div class="ls" >
          <div class="hvr-eff">
            <img class="img-res" src="<?=base_url()?>assets/img/lessons/8.jpg" class="img">

          </div>
          <div class="les-text">
            <b class="big">Laravel</b>
           <p class="cdt">Տևողությունը 1 ամիս <br> ուսման վարձը 40.000 դրամ  </p>
           <p data-lsMore="lrvl">ուսումնական պլան</p>
          </div>
        </div>
        <div class="ls">
          <div class="hvr-eff">
            <img class="img-res" src="<?=base_url()?>assets/img/lessons/9.jpg" class="img">

          </div>
          <div class="les-text">
            <b class="big">Wordpress</b>
            <p class="cdt">Տևողությունը 1 ամիս <br> ուսման վարձը 30.000 դրամ  </p>
            <p  data-lsMore="wdp">ուսումնական պլան</p>
          </div>
        </div>    

   		</div>
      <section class="lesson-fade">
      <div class="lesson-more ls-red" data-lsMore="html">
          <i class="material-icons">clear</i>
          <div class="ls-container">
            <section class="ls-col fl-between">
            <h5>HTML/CSS</h5>
              <ul type="circle">
                <li>Վեբ կայքերի աշխատանքի սկզբունքները</li>
                <li>HTML լեզվի ընդհանուր կառուցվածքը</li>
                <li>Աղյուսակներ, ցանկեր եւ ենթացանկեր</li>
                <li>HTML5 -ի առանձնահատկությունները, media</li>
                <li>CSS ֆունդամենտալ, սելեկտորներ</li>
                <li>CSS Box Model և Layout</li>
                <li>Pseudo եւ կոմպլեքս սելեկտորներ, flexbox</li>
                <li>2D/3D անիմացիայի կիրառում կայքերում </li>
                <li>Positioning</li>
                <li>Responsive դիզայն</li>
                <li>Bootstrap ֆունդամենտալ, Grid System </li>
                <li>Materialize, CSS -ի այլ framework-ներ</li>
                <li>CSS/CSS3 -ի ամփոփում</li>
              </ul>
            <button data-target="modal1" data-lesson="HTML/CSS"  class="btn waves-effect light white">Գրանցվել</button>
            </section>
          </div>
      </div>
      <div class="lesson-more ls-green" data-lsMore="js">
          <i class="material-icons">clear</i>
          <div class="ls-container">
          <section class="ls-col fl-between">
          <h5 class="cdt">JavaScript/jQuery</h5>
          <ul>
            <li>Ալգորիթմներ, փոփոխականներ եւ տիպեր</li>
            <li>Պայմանական օպերատոր</li>
            <li>Ցիկլեր</li>
            <li>Event-ներ</li>
            <li>Զանգվածներ: Տողային տիպ</li>
            <li>Դինամիկ ծրագրավորման մեթոդը JavaScript -ում</li>
            <li>Խաղերի ծրագրավորման օրինակներ</li>            
            <li>Ինտերվալներ, Անիմացիայի ձեւավորումը</li>
            <li>Ռեկուրսիա, անանուն ֆունկցիաներ, լյամբդա արտահայտություն.</li>
            <li>jQuery, Event-ներ, selector-ներ</li>
            <li>jQuery - get/set HTML -ում</li>
            <li>jQuery - Traversing</li>
            <li>jQuery - Դինամիկ ծրագրավորում</li>
            <li> Օբյեկտներ</li>
            <li>Հասանելիության տիպեր, ընդլայնում</li>
            <li>Կլասներ, ստատիկ եւ աբստրակտ դաշտեր, ժառանգում</li>
            <li> Օբյեկտի տեսանելիություն, ռեֆելեքսիա, նախատիպ (prototype)</li>
            <li>JavaScript -ի API-ներ (Highcharts, Google Map...)</li>
            <li>Գրաֆիկան JavaScript-ում, Canvas</li>
            <li>JS -ով Real App-ների մշակման օրինակներ</li>
          </ul><br><br>
          <button data-target="modal1" data-lesson="JavaScript/jQuery"  class="btn waves-effect light white">Գրանցվել</button>
          </section>
          </div>
      </div>
       <div class="lesson-more ls-green" data-lsMore="php">
          <i class="material-icons">clear</i>
          
          <div class="ls-container fl-around">
          <section class="ls-col">
            <h5>PHP/OOP</h5>
            <ul>
              <li> PHP  ֆունդամենտալ, associative զանգվածներ</li>
              <li> ֆունկցիոնալ ծրագրավորում PHP -ում:</li>
              <li>Վիրտուալ հոստ եւ հtaccess ֆայլեր</li>
              <li>Ֆայլային համակարգեր</li>
              <li>Տվյալների ֆիլտրացիա</li>
              <li>OOP. հասանելիության տիպեր, կոմպոզիցիա</li>
              <li>Ինկապսուլացիա. Magic մեթոդներ</li>
              <li>Ժառանգում, ժառանգման բացառում</li>
              <li>Պոլիմորֆիզմ</li>
              <li>Աբստրակցիա եւ ինտերֆեյս</li>
              <li>Session եւ Cookie </li>
              <li>AJAX:</li>
              <li>Սինքրոնիզացիա</li>
              <li>VCS համակարգեր</li>
              <li>Design Pattern</li>
            </ul>
          </section>

          <section class="ls-col">
            <h5>Տվյալների Բազաներ/MySQԼ</h5>
            <ul >
            <li> Տվյալների բազաների կառավարում</li>
            <li> Աղյուսակներ. Որոնողական հարցումներ</li>
            <li> Կառավարող հարցումները (CRUD) MySQL -ում</li>
            <li> Տվյալների բազաների մոդելներ:</li>
            <li> Աղյուսակների կապակցում (Join)</li>
            <li> Ենթահարցումներ եւ կոմպլեքս հարցումներ</li>
            <li> Տվյալների խմբավորում</li>
            <li> ֆունկցիաներ եւ պրոցեդուրաներ</li>
            <li> Տրիգերներ</li>
            <li>. (Index,View),տվյալների միավորում</li>
            <li>. PHP եւ MySQL համալիրը: PDO եւ mysqli.</li>
            </ul>
            <button data-target="modal1" data-lesson="PHP/OOP"  class="btn waves-effect light white">Գրանցվել</button>
         </section>
        </div>

      </div>
      <div class="lesson-more ls-yellow" data-lsMore="mvc">
          <i class="material-icons">clear</i>
          
          <div class="ls-container">
          <section class="ls-col fl-between">
          <h5 class="cdt">MVC & Codeigniter</h5>
          <ul>
            <li>MVC design pattern: </li>
            <li>Codeigniter 3 Framework, routing</li>
            <li>Controller-View համակարգը </li>
            <li>Model-ների ստեղծում եւ կառավարում</li>
            <li>Loader-ները Codeigniter -ում</li>
            <li>Library-ների կիրառում Codeigniter -ում</li>
            <li>Ցանցային ծրագրավորման գրադարաններ</li>
            <li>Ռեսուրսներ եւ Auto loading</li>
            <li> Cache -ի կիրառում</li>
          </ul>
          <button data-target="modal1" data-lesson="MVC & Codeigniter"  class="btn waves-effect light white">Գրանցվել</button>
          </section>
          </div>
      </div>
      <div class="lesson-more ls-green" data-lsMore="wdp" >
          <i class="material-icons">clear</i>
          
          <div class="ls-container">
          <section class="ls-col fl-between">
          <h5 class="cdt">Wordpress</h5>
          <ul>
            <li> CMS -ների ներածություն, Wordpress` կառուցվածքը եւ կառավարումը</li>
            <li> Գաղափար Plugin-ների մասին</li>
            <li>Action եւ Filter Hook-եր, dbDelta ֆունկցիան</li>
            <li> Admin մենյու եւ ենթամենյուներ եւ մեդիա ֆայլերի կիրառում</li>
            <li> AJAX -ը Wordpress -ում</li>
            <li> Shortcode -ի կիրառում</li>
            <li>WP Query կլասը, ֆիլտրացիոն հուկեր</li>
            <li> Տաքսոնոմիաներ</li>
          </ul>
          <button data-target="modal1" data-lesson="Wordpress"  class="btn waves-effect light white">Գրանցվել</button>
          </section>
          </div>
      </div>
       <div class="lesson-more ls-blue" data-lsMore="ng2">
          <i class="material-icons">clear</i>

          <div class="ls-container">
          <section class="ls-col fl-between">
          <h5 class="cdt">Angular2, TypeScript</h5>
          <ul>
            <li>Դինամիկ, ստատիկ և պայմանական տիպեր</li>
            <li>Թվարկումներ, void և value-type ֆունկցիաներ</li>
            <li>Ինտերֆեյսներ</li>
            <li>Դեկորատորներ</li>
            <li>Angular CLI</li>
            <li>Model Binding</li>            
            <li>Կոմպոնենտատյին ծրագրավորում Angular2 -ում</li>
            <li>Դիրեկտիվների ընդլայնում և նախագծում</li>
            <li>http հարցումներ</li>
            <li>Մոդուլյար ծրագրավորում</li>
            <li>Generic ծրագրավորում</li>

         
          </ul>
          <button data-target="modal1" data-lesson="AngularJS"  class="btn waves-effect light white">Գրանցվել</button>
          </section>
          </div>
      </div>
       <div class="lesson-more ls-blue" data-lsMore="lrvl">
          <i class="material-icons">clear</i>
          
          <div class="ls-container">
          <section class="ls-col fl-between">
          <h5 class="cdt">Laravel</h5>
          <ul>
            <li> Сomposer , Laravel -ի կոնֆիգուրացիան</li>
            <li> Service Container, Service Provider</li>
            <li>Facades եւ Contracts գաղափարները </li>
            <li> Routing, Layout եւ Blade Template—ներ</li>
            <li> Տվյալների հոսքը Controller-View համակարգում</li>
            <li> Մոդելները Laravel -ում, Eloquent</li>
            <li> Auth համակարգի ինտեգրացիան</li>
            <li> Տվյալների բազաների միգրացիաներ</li>
            <li> Տվյալների բազաների մոդելներ եւ Eloquent ORM</li>
            <li> Middleware, Events եւ ներկառուցված գրադարաններ</li>
          </ul>
          <button data-target="modal1" data-lesson="Laravel"  class="btn waves-effect light white">Գրանցվել</button>
            </section>
            </div>
      </div>
      <div class="lesson-more ls-red" data-lsMore="node" >
          <i class="material-icons">clear</i>
          
          <div class="ls-container">
          <section class="ls-col fl-between">
          <h5 class="cdt">Node.js</h5>
          <ul>
            <li>OOP սկզբունքները ECMAScript6 -ում</li>            
            <li>Մոդուլյար ծրագրավորում </li>
            <li>Ֆայլային համակարգեր</li>
            <li>Ասինքրոն և սինքրոն հոսքեր</li>            
            <li>Express.js ներածություն, Կոնֆիգուրացիա</li>
            <li>Կառուցվածքային ծրագրավորում Express -ում</li>
            <li>Template Engine, EJS եւ Jade, partial templates</li>
            <li>Socket.IO մոդուլը</li>
            <li>Հոսքերի սինքրոնիզացիա, Promise</li>
            <li>Real Time App-ների մշակման օրինակներ</li>
            <li>Middleware, Events</li>
            <li>VCS համակարգեր</li>

          </ul>
          </section>

          <section class="ls-col fl-between">
          <h5 class="cdt">React.js</h5>
          <li>React.js ներածություն: Virtual DOM, JSX. </li>
          <li>Կոմպոնենտատյին ծրագրավորում </li>
          <li>Ռեակտիվ կոմպոնենտների մշակում</li>
          <li>Տվյալների հոսքեր: Props և state </li>
          <li>Էլեմենտների ստեղծում, Render</li>
          <li>Props Validation</li>
          <li>ReactJS - Forms, Events, Router </li>
          <li>Կոմպոնենտների կառավարումը</li>
          <li>ReactJS - Flux concept, Animations</li>
          <li>Բարձր կարգի կոմպոնենտներ</li><br><br>
          <button data-target="modal1" data-lesson="Node.js"  class="btn waves-effect light white">Գրանցվել</button>
            </section>
            </div>
      </div>
      </section>
      </div>


    <h2 class=" center r-heading">Առաջիկա դասընթացներ</h2>
    <div class="reg-cards" id="reg-cards">
    	<div class="cd">
    		<div class="cd-img" style="background-image: url(<?= base_url().'/assets/img/lessons/1.jpg' ?>);">
    			
    		</div>
    		<div class="cd-text">
    			<div class="cd-cont">
    				<h6 class="big">HTML/CSS</h6>
    				<b class="cdt">տևողությունը՝ 1 ամիս</b>
    				<p class="cdt">
                Դասընթացը մեկնարկելու է հուլիսի 10-ից, 2017թ     
            </p>
    				<h6 class="big" style="color:#35ba35">30.000 դրամ</h6>
    			</div>
    			
    			<div class="cd-footer" data-target="modal1" data-lesson="html/css">
    				  <h6 class="cdt">ԳՐԱՆՑՎԵԼ</h6>
    			</div>
    		</div>
    	</div>
    	<div class="cd">

        <div class="cd-img" style="background-image: url(<?= base_url().'/assets/img/lessons/6.jpg' ?>);">
          
        </div>
        <div class="cd-text">
          <div class="cd-cont">
            <h6 class="big">PHP/MySQL</h6>
            <b class="cdt">տևողությունը՝ 2 ամիս</b>
            <p class="cdt">Դասընթացի մեկնարկը՝ հուլիսի 10, 2017թ</p>
            <h6 class="big" style="color:#35ba35">60.000 դրամ</h6>
          </div>
          
          <div class="cd-footer" data-target="modal1" data-lesson="angular.js">
            <h6 class="cdt">ԳՐԱՆՑՎԵԼ</h6>

          </div>

        </div>
      </div>
      <div class="cd">
        <div class="cd-img" style="background-image: url(<?= base_url().'/assets/img/lessons/2.jpg' ?>);">
          
        </div>
        <div class="cd-text">
          <div class="cd-cont">
            <h6 class="big">JavaScript/jQuery</h6>
            <b class="cdt"> տևողությունը՝ 2 ամիս</b>
            <p class="cdt">Դասընթացի մեկնարկը՝ հուլիսի 10, 2017թ</p>
            <h6 class="big" style="color:#35ba35">60.000 դրամ</h6>
          </div>
          
          <div class="cd-footer" data-target="modal1" data-lesson="Javascript/jQuery">
            <h6 class="cdt">ԳՐԱՆՑՎԵԼ</h6>


          </div>
        </div>
      </div>
      <div class="cd">
        <div class="cd-img" style="background-image: url(<?= base_url().'/assets/img/lessons/10.jpg' ?>);">
          
        </div>
        <div class="cd-text">
          <div class="cd-cont">
            <h6 class="big">Node.js</h6>
            <b class="cdt"> տևողությունը՝ 2 ամիս</b>
            <p class="cdt">Մեկնարկը՝ հուլիսի 10, 2017թ</p>
            <h6 class="big" style="color:#35ba35">60.000 դրամ</h6>
          </div>
          
          <div class="cd-footer" data-target="modal1" data-lesson="php/mysql">
            <h6 class="cdt">ԳՐԱՆՑՎԵԼ</h6>
          </div>
        </div>
      </div>
    	
    </div>
    <div class="about col s12">
    	<div class="about-icons">
    		<div class="abt-icon abt-icon-active" data-info="h-1">
    			<i class="material-icons ">school</i>
    			<h5 align="center" class="cdt">ՈՒՍՈՒՑՈՒՄ</h5>
    		</div>
    		<div class="abt-icon" data-info="h-2">
          <i class="material-icons ">star_half</i>
    			<h5 align="center" class="cdt">ՊՐԱԿՏԻԿԱ</h5>

    		</div>
    		<div class="abt-icon" data-info="h-3">
          <i class="material-icons ">business_center</i>
    			<h5 align="center" class="cdt">ԱՇԽԱՏԱՆՔ</h5>

    		</div>
    		<div class="abt-icon" data-info="h-4">
          <i class="material-icons ">attach_money</i>
    			<h5 align="center" class="cdt">ՈՒՍՄԱՆ ՎԱՐՁԻ ՎԵՐԱԴԱՐՁ</h5>

    		</div>
    		
    	</div>
      <div class="abt-txt-holder">
      	<div class="abt-text show" data-info="h-1">
          <p class="res-p">Մեր ուսումնական կենտրոնի երկու մասնաճյուղերում դասավանդում են փորձառու ծրագրավորողներ: Ուսումնական պլանը համապատասխանեցված է ՏՏ ոլորտի արդի պահանջներին: Ուսանողների զգալի մասը աշխատանքի են ընդունվում դասընթացը դեռ չավարտած: Յուրաքանչյուր մոդուլի ընթացքում ուսանողների կողմից մշակվում է պրոյեկտ, որը հետագայում հանդիսանալու է նրանց պորտֆոլիոյի մասը:
          Տեսական նյութի ուսուցմանը զուգահեռ ուսանողների հետ քննարկվում են բոլոր այն հարցերը, որոնք կարող են հանդիպել նրանց տարբեր ՏՏ ընկերություններում հարցազրույցներ անցնելիս:
  			</p>
      	</div>
        <div class="abt-text hidden" data-info="h-2">
          <p class="res-p">
            Ուսուցման ընթացքում բարձր առաջադիմությամբ ուսանողները Profit Development Company-ի կողմից ստանում են հնարավորություն մասնակցելու գործող պրոյեկտների մշակմանն ու ստեղծմանը, զարգացնում ու կատարելագործում են իրենց գիտելիքները, ստանում փորձ, որը ներկայումս յուրաքանչյուր սկսնակ ծրագրավորողի համար, խիստ կարևոր գործոն է:


        </p>
        </div>
        <div class="abt-text hidden" data-info="h-3">
          <p class="res-p">

              Բոլոր այն ուսանողները, ովքեր հաջողությամբ են անցել պրակտիկայի փուլը Պրոֆայթի ընկերությունում, ստանում են աշխատանքի առաջարկ: 
          </p>
        </div>
        <div class="abt-text hidden" data-info="h-4">
          <p class="res-p">Պրոֆայթի ընկերությունում հրաշալի ավանդույթ է դարձել բարձր առաջադիմությամբ ուսանողներին աշխատանքով ապահովելը: Այսպիսով ուսանողները, ովքեր կցուցաբերեն գերազանց առաջադիմություն կստանան աշխատանքի առաջարկ Պրոֆայթի ընկերությունում եւ ետ կստանան իրենց ուսման վարձն առնվազն 50% -ի չափով:

          Պրոֆայթին առաջինն է Հայաստանում, որ հնարավորություն է տալիս ստանալ կրթություն նաեւ ապառիկ ճանապարհով: Inecobank -ի հետ համագործակցության արդյունքում՝ այժմ մեր ուսանողները կարող են ստանալ որակյալ կրթություն ամենամատչելի պայմաններով: Բացի այդ, ուսանողներն, ովքեր աշխատանքի կանցնեն մեր ընկերությունում ետ կստանան իրենց վճարած ուսման վարձը՝ 50-100% -ով:

        </p>
        </div>
        
      </div>
    	<div class="abt-footer">
    		<ul class="abt-ft-scroll">
    		<article class="abt-footer-section" style="background-image: url(<?=base_url()?>assets/img/students/a55.jpg);">
    		</article>
    		<article class="abt-footer-section" style="background-image: url(<?=base_url()?>assets/img/students/a2.jpg)">
    		</article>
    		<article class="abt-footer-section" style="background-image: url(<?=base_url()?>assets/img/students/p3.jpg)">
    		</article>
    		<article class="abt-footer-section" style="background-image: url(<?=base_url()?>assets/img/students/a44.jpg)">
    		</article>
        <article class="abt-footer-section" style="background-image: url(<?=base_url()?>assets/img/students/a7.jpg)">
        </article>
    		<article class="abt-footer-section" style="background-image: url(<?=base_url()?>assets/img/students/karen.jpg)">
    		</article>
        <article class="abt-footer-section" style="background-image: url(<?=base_url()?>assets/img/students/a66.jpg)">
        </article>
        <article class="abt-footer-section" style="background-image: url(<?=base_url()?>assets/img/students/a3.jpg)">
        </article>
        <article class="abt-footer-section" style="background-image: url(<?=base_url()?>assets/img/students/a1.jpg)">
        </article>
    		</ul>
      </div>
      <div class="abt-scroll">
        <input type="range" id="scroll-images" min="0">
      </div>
    </div>
    <div class="teachers">
    	<header>
    		<h4 class="cdt r-heading">ՈՎՔԵ՞Ր ԵՆ ԴԱՍԱՎԱՆԴՈՒՄ ՄԵԶ ՄՈՏ</h4>
    	</header>
    	<div class="teachers-list">
    		<section>
          <div class="teacher-img" style="background-image: url(<?=base_url()?>assets/img/students/a4.jpg)">
            
          </div>
    			<h6 class="res-p">Արմեն Մարջինյան</h6>
    		</section>
    		<section>
        <div class="teacher-img" style="background-image: url(<?=base_url()?>assets/img/students/a5.jpg)">
            
          </div>
    			<h6 class="res-p">Անահիտ Պողոսյան</h6>
    		</section>
    		<section>
          <div class="teacher-img" style="background-image: url(<?=base_url()?>assets/img/students/ed1.jpg)">
            
          </div>
    			<h6 class="res-p">Էդուարդ Հովհաննիսյան</h6>
    		</section>
    	</div>
    </div>

 

    <div class="comming-soon">
      <section class="countdown">
        <h4 class="flow-text cdt">Ծրագրավորում 0 -ից {} - Հուլիսի 10</h4>
        <span class="flow-text">HTML / CSS</span>
        <h3 class="flow-text cdt">մեկնարկին մնացել է</h3>
        <ul class="timer">
            <li class="time-box">
                <header class="red darken-1">
                    <span></span>
                </header>
                <footer>
                    <h4></h4>
                </footer>
            </li>
             <li class="time-box">
                <header class="purple lighten-1">
                    <span></span>
                </header>
                <footer>
                    <h4></h4>
                </footer>
            </li>
            <li class="time-box">
                <header class="light-green">
                    <span></span>
                </header>
                <footer>
                    <h4></h4>
                </footer>
            </li>
           
            <li class="time-box">
                <header class="yellow">
                    <span></span>
                </header>
                <footer>
                    <h4></h4>
                </footer>
            </li>
            
        </ul>
        </footer>
      </section>
      <section class="cmg-reg" id="registration">
          <h3 class="flow-text">Գրանցվել դասընթացին</h3>
          <div class="input-field ">
            <input  id="first_name" type="text" class="validate">
            <label for="first_name">Անուն Ազգանուն</label>
          </div>
          <div class="input-field ">
            <input  id="email" type="text" class="validate">
            <label for="email">Էլ. հասցե</label>
          </div>
          <div class="input-field ">
            <input  id="phone" type="text" class="validate">
            <label for="phone">հեռախոսահամար</label>
          </div>
          <button data-lesson="ամբողջական կուրս" class="btn waves-effect waves-light blue lighten-1 reg_to_all">Հաստատել
          <i class="material-icons right">send</i>
        </button> 
      </section>
    </div>

    <div class="feedback" id="feedback">
      <h3 class="center r-heading">ՈՒՍԱՆՈՂՆԵՐԸ ՄԵՐ ՄԱՍԻՆ</h3>
       <div class="fb">
          <section class="student-fb">
              <header>
                  <div class="student-img" style="background-image: url(<?= base_url()?>/assets/img/students/arman.jpg)">
             
                  </div>
               
                  <section>
                    <b class="res-p center">ԱՐՄԱՆ ՀՈՎՀԱՆՆԻՍՅԱՆ</b>
                    <small class="res-p">պրակտիկանտ, Profit D.C.</small>
                  </section>
              </header>
              <footer>
                  <p class="res-p center">
                    Ուրախ եմ, որ ինձ ընձեռվեց հնարավորություն, մասնակցելու ուսումնական պրակտիկայի:Ներկայումս Պրոֆայթի ընկերության ծրագրավորողների հետ միասին աշխատում եմ տարբեր պրոյեկտների վրա՝ զարգացնելով գործնական հմտություններս:
                  </p>
              </footer>
          </section>
          <section class="student-fb">
              <header>
                  <div class="student-img" style="background-image: url(<?= base_url()?>/assets/img/students/tigran.jpg)">

             
                  </div>
                  <section>
                    <b class="res-p center">ՏԻԳՐԱՆ ԿՈՇԵՑՅԱՆ</b>
                    <small class="res-p">պրակտիկանտ, Profit D.C.</small>
                  </section>
              </header>
              <footer>
                  <p class="res-p center">
                    Դասընթացին զուգընթաց, այժմ, մասնակցում եմ ուսումնական պրակտիկայի, ինչի շնորհիվ դասընթացի ժամանակ ստացածս գիտելիքնրը, կարող եմ կիրառել գործնականում:
                  </p>
              </footer>
          </section>
          <section class="student-fb">
              <header>
                  <div class="student-img" style="background-image: url(<?= base_url()?>/assets/img/students/arshaluys.jpg)"></div>


                  <section>
                    <b class="res-p center">ԱՐՇԱԼՈՒՅՍ ԿԻՐԱԿՈՍՅԱՆ</b>
                    <small class="res-p">Web Developer, Profit D.C.</small>
                  </section>
              </header>
              <footer>
                  <p class="res-p center">
                    Ուսումնական կենտրոնում սովորելու ընթացքում ինձ հնարավորություն տրվեց անցնել պրակտիկա Պրոֆայթիում: Երկամսյա ուսումնական պրակտիկայից հետո, ընդգրկվել եմ աշխատանքային պրակտիկայի խմբում և այժմ աշխատում եմ ընկերության պրոյեկտների վրա:
                  </p>
              </footer>
          </section>
          <section class="student-fb">
              <header>
                  <div class="student-img" style="background-image: url(<?= base_url()?>/assets/img/students/hrach-avg.jpg)"></div>

                  <section>
                    <b class="res-p center">ՀՐԱՉ ԱՎԱԳՅԱՆ</b>
                    <small class="res-p">Web Developer, Profit D.C.</small>
                  </section>
              </header>
              <footer>
                  <p class="res-p center">
                    Ուրախ եմ, որ դասընթացին զուգահեռ ունեցա հնարավորություն անցնելու ուսումնական պրակտիկա Պրոֆայթի ընկերությունում, որտեղ գիտելիքներս կարողացա կիրառել տարբեր պրոյեկտներում: Այժմ ընդգրկված եմ աշխատանքային պրակտիկայի խմբում:
                  </p>
              </footer>
          </section>
          <section class="student-fb">
              <header>
                  <div class="student-img" style="background-image: url(<?= base_url()?>/assets/img/students/hrach.jpg)"></div>


                  <section>
                    <b class="res-p center">ՀՐԱՉ ԵՐԵՄՅԱՆՑ</b>
                    <small class="res-p">Աշխատում է որպես ծրագրավորող</small>
                  </section>
              </header>
              <footer>
                  <p class="res-p center">
                   Ավարտելով դասընթացը` կարող եմ ասել, որ կատարված էր ճիշտ ընտրություն եւ իմ գիտելիքները ինձ լիովին հնարավորություն են տալիս աշխատել որպես վեբ ծրագրավորող:
                  </p>
              </footer>
          </section>
          <section class="student-fb">
              <header>
                  <div class="student-img" style="background-image: url(<?= base_url()?>/assets/img/students/aramyan.jpg)"></div>

                  <section>
                    <b class="res-p center">ԱՆԱՀԻՏ ԱՐԱՄՅԱՆ</b>
                    <small class="res-p">Աշխատում է որպես ծրագրավորող</small>
                  </section>
              </header>
              <footer>
                  <p class="res-p center">
                    Եթե ցանկանում եք դառնալ ծրագրավորող, ստանալ որակյալ կրթություն,ապա միանշանակ խորհուրդ եմ տալիս ընտրել Պրոֆայթին, որտեղ բարձրակարգ մասնագետների շնորհիվ կհասնեք ցանկալի արդյունքի:
                  </p>
              </footer>
          </section>
          <section class="student-fb">
              <header>
                  <div class="student-img" style="background-image: url(<?= base_url()?>/assets/img/students/karen2.jpg)"></div>

                  <section>
                    <b class="res-p center">ԿԱՐԵՆ ԲԱՂԴԱՍԱՐՅԱՆ</b>
                    <small class="res-p">Աշխատում է որպես ծրագրավորող</small>
                  </section>
              </header>
              <footer>
                  <p class="res-p center">
                    Սովորելով Պրոֆայթի ընկերությունում՝ կարող եմ ասել, որ այստեղ ինձ հաջողվեց ոչ միայն ընդլայնել գիտելիքներս, այլ նաեւ զարգացնել աշխարհայացքս: Եթե գիտելիք, ապա ՊՐՈՖԱՅԹԻ.
                  </p>
              </footer>
          </section>
          <section class="student-fb">
              <header>
                  <div class="student-img" style="background-image: url(<?= base_url()?>/assets/img/students/sargis.jpg)"></div>

                  <section>
                    <b class="res-p center">ՍԱՐԳԻՍ ԴԱՎԹՅԱՆ</b>
                    <small class="res-p">Աշխատում է որպես ծրագրավորող</small>
                  </section>
              </header>
              <footer>
                  <p class="res-p center">
                    Պրոֆայթի ընկերությունում արդարացվեցին բոլոր սպասելիքներս նախ՝ որակյալ կրթություն ստանալու, ապա նաեւ մարդկային փոխհարաբերությունների առումով:
                  </p>
              </footer>
          </section>
          <section class="student-fb">
              <header>
                  <div class="student-img" style="background-image: url(<?= base_url()?>/assets/img/students/narek.jpg)"></div>

                  <section>
                    <b class="res-p center">ՆԱՐԵԿ ԱՆՏԱԲՅԱՆ</b>
                    <small class="res-p">Աշխատում է որպես ծրագրավորող</small>
                  </section>
              </header>
              <footer>
                  <p class="res-p center">
                    Պրոֆայթին այն ուսումնական կենտրոնն է, որտեղ հնարավոր է կարճ ժամանակահատվածում ձեռք բերել խորը գիտելիք: Հստակ սահմանված ուսումնական պլան, նյութի մատչելի բացատրություն եւ որակյալ կրթություն:
                  </p>
              </footer>
          </section>
          <section class="student-fb">
              <header>
                  <div class="student-img" style="background-image: url(<?= base_url()?>/assets/img/students/anahit-m.jpg)"></div>


                  <section>
                    <b class="res-p center">ԱՆԱՀԻՏ ՄՈՎՍԻՍՅԱՆ</b>
                    <small class="res-p">Front End ծրագրավորող</small>
                  </section>
              </header>
              <footer>
                  <p class="res-p center">
                    Պրոֆայթիում տիրող մթնոլորտը եւ բարյացակամ վերաբերմունքն ինձ վստահություն ներշնչեցին, ինչը ամրապնդվեց դասավանդողի՝ մասնագիտական և մարդկային բարձր արժանիքներով օժտված մասնագետի, շնորհիվ:
                  </p>
              </footer>
          </section>
       </div>
    </div>


       <div class="news">
      <div class="grid-example col s12 center ">
        <h3 class=" r-heading">ՀԱՋՈՂՎԱԾ ՊԱՏՄՈՒԹՅՈՒՆՆԵՐ</h3>
      </div>
      <div class="new-lessons">
          <article>
          <section class="n-img" style="background-image: url(<?= base_url()?>/assets/img/students/haykaz.jpg);">

          </section>
          <section class="n-cont">
            <button class="mid-btn green lighten-1">ՀԱՅԿԱԶ</button>
            <span class="res-p"><b class="res-p">Աշխատում է որպես ծրագրավորող</b></span>
            <p class="res-p">Պրոֆայթի ուսումնական կենտրոնի մասին իմացել եմ ֆեյսբուքյան հայտարարության միջոցով: Ներկա գտնվելով առաջին մի քանի դասերին, իրապես, հասկացա, որ նախապատվությունը պետք է տալ պրակտիկ դասերին: Դասընթացի ընթացքում ինձ հատկապես դուր եկավ այն, որ ուսումնական պլանն իր մեջ ներառում է սկսնակ ծրագրավորողին անհրաժեշտ բոլոր նյութերն ու տեխնոլոգիաները: Այսօր արդեն, աշխատելով որպես ծրագրավորող, կարող եմ ասել, որ դասընթացի ընթացքում մենք լուծում էինք այնպիսի խնդիրներ, որոնք իսկապես կիրառվում են ծրագրավորման պրոցեսում: Ուրախ եմ, որ դասընթացի արդյունքը եղավ այն, որ սկսեցի աշխատել Պրոֆայթի ընկերությունում որպես ծրագրավորող:</p>
          </section>
        </article>
        <article>
          <section class="n-img" style="background-image: url(<?= base_url()?>/assets/img/students/dav.jpg);">

          </section>
          <section class="n-cont">
            <button class="mid-btn green lighten-1">ԴԱՎԻԹ</button>
            <span class="res-p"><b class="res-p">Աշխատում է որպես ծրագրավորող</b></span>
            <p class="res-p">Պրոֆայթի ուսումնական կենտրոնի մասին իմացել եմ նրանց ֆեյսբուքյան էջից: Ներկա լինելով առաջին դասին՝ հասկացա, որ գտնվում եմ ճիշտ վայրում որակյալ կրթություն և լավ մասնագետ դառնալու համար: Սովորելուս ողջ ընթացքում ինձ գրավում էր մոտիվացնող մթնոլորտը, ինչպես նաև դասավանդողի հաճելի, դրական և միաժամանակ պահանջկոտ վերաբերմունքն ուսանողների նկատմամբ: Ավարտելով դասընթացը՝ կարող եմ ասել, որ Պրոֆայթին ինձ հանդիպած միակ ուսումնական կենտրոնն է, որտեղ իրապես հնարավոր է ստանալ բարձր և որակյալ գիտելիք: Դասընթացի ավարտին աշխատանքի անցա որպես վեբ ծրագրավորող և մինչ օրս ստացածս ողջ գիտելիքը կիրառում եմ իմ ամենօրյա աշխատանքում:</p>
          </section>
        </article>
        <article>
          <section class="n-img" style="background-image: url(<?= base_url()?>/assets/img/students/kar.png);">

          </section>
          <section class="n-cont">
            <button class="mid-btn green lighten-1">ԿԱՐԵՆ</button>
            <span class="res-p"><b class="res-p">Աշխատում է որպես ծրագրավորող</b></span>
            <p class="res-p">Պրոֆայթի ուսումնական կենտրոնի մասին իմացել եմ ինտերնետի օգնությամբ: Մասնակցել եմ առաջին դասին, տպավորություններս գերազանց էին: Դասընթացի ընթացքում ինձ հատկապես դուր է եկել դասավանդողի՝ ուսանողին հարգելու և նրան անընդհատ նոր գիտելիք փոխանցելու ձգտումը: Ես բոլորին խորհուրդ կտամ սովորել Պրոֆայթի ուսումնական կենտրոնում, քանի որ վստահ եմ, որ դասընթացի ավարտին ուսանողի գիտելիքները բավարար կլինեն junior developer աշխատելու համար: Պրոֆայթիում ստացածս տեսական և գործնական գիտելիքների շնորհիվ կարողացա բարեհաջող կերպով հանձնել աշխատանքի ընդունման բանավոր և գործնական քննությունը, և մինչ օրս աշխատում եմ որպես վեբ ծրագրավորող:</p>
          </section>

        </article>
      
       
      </div>

    </div>



<div id="modal1" class="modal">
    <div class="modal-content">
      <h4 class="r-heading">Գրանցում</h4>
      <del></del>
      <p class="res-p">Խնդրում ենք լրացնել Ձեր անունը, ազգանունը, հեռախոսահամարը և էլ-փոստը:
      Lրացուցիչ դաշտում կարող եք նշել, թե որ մասնաճյուղում (Տիգրան Մեծ, թե Մոսկովյան) և երբ եք ցանկանում սկսել դասընթացը, Ձեզ հարմար օրերն ու ժամերը, տեղադրել Ձեզ հետաքրքրող հարցեր:
      <br>
    <br><p class="res-p">Հաստատումից հետո մեր մասնագետը կկապվի Ձեզ հետ: </p>
      </p>
        <p class="error res-p"></p><br><br>
    
    <section class="reg-form">

    <div class="row">
          <div class="input-field col s12 m12">
            <input id="name" type="text" class="validate" tabindex="1">
            <label for="name" class="">Անուն Ազգանուն</label>
          </div>
            <div class="input-field col s12 m12">
            <input id="reg_phone" type="text" class="validate" tabindex="2">
            <label for="reg_phone">Հեռախոսահամար</label>
          </div>
      </div>
      <div class="row">
          <div class="input-field col s12 m12">
              <textarea id="text" class="materialize-textarea" tabindex="3"></textarea>
              <label for="text">Լրացուցիչ</label>
          </div>
          <div class="input-field col s12 m12">
            <input id="reg_email" type="email" class="validate" tabindex="4">
            <label for="reg_email">Էլ-փոստ</label>
          </div>
      </div>
      </div>

      </section>
      
    <div class="modal-footer">
      <a tabindex="5"  class="modal-action modal-close waves-effect waves-green btn-flat">Չեղարկել</a>
      <a tabindex="6" class="modal-action accept waves-effect waves-green btn-flat register">Հաստատել</a>

    </div>
  </div>

    <h3 class="r-heading center" id="find-us">Ինչպե՞ս գտնել մեզ</h3>
    <div class="coordinating">
    <div id="map-select">
      <div class="user-pos">
            <input type="text" placeholder="Նշեք Ձեր գտվելու վայրը" id="map-search">
      </div>
        <div class="our-pos">
            <select id="our-crd">
              <option value="Մոսկովյան 3">Մոսկովյան</option>
              <option value="Տիգրան Մեծ 49">Տիգրան Մեծ</option>
            </select>
             <button class="btn green lighten-1 show-direction waves-effect waves-light">Ցուցադրել ճանապարհը</button>
        </div>
    </div>
    <div id="map"></div>
    </div>
    <div class="bottom-side">
        <section class="btm-logo">
            <img src="<?=base_url()?>assets/img/logo-2.png" alt="">
        </section>
        <section class="find-us">
          <section>
            <i class="material-icons info">phone</i>
            <h6> 010 574 273, 098 776 348</h6>
          </section>
         
          <section>
            <i class="material-icons info">input</i>
            <h6>Տիգրան Մեծ 49, Մոսկովյան 3</h6>
          </section>

             
        </section>

    </div>
    <input type="hidden" id="base" value="<?= base_url(); ?>" name="">
    <div class="progress">
      <div class="indeterminate"></div>
    </div>


    <a id="to-top" class="btn-floating btn-large light-green darken-2">
      <i class="large material-icons">navigation</i>
    </a>

    </div>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZfRbgZPIeMiDsD0Bl58vhqAh8wUX0pbc&libraries=places" > </script>

<script src="<?= base_url().'/assets/js/script.js' ?>"></script>

</html>


